package id.chalathadoa.myapplication

import android.app.Person
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import id.chalathadoa.myapplication.adapter.PersonAdapter
import id.chalathadoa.myapplication.databinding.FragmentTop3Binding
import id.chalathadoa.myapplication.model.person

class FragmentTop3 : Fragment() {
    private var _binding: FragmentTop3Binding? = null
    private val  binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTop3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }
    private val listPerson = arrayListOf<person>(
        person("Ana", 1234),
        person("lisa", 8765),
        person("oca",2343)
    )
    private fun initRecyclerView(){
        val PersonAdapter = PersonAdapter(listPerson)
        binding.rvSatu.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = PersonAdapter
        }
    }
}