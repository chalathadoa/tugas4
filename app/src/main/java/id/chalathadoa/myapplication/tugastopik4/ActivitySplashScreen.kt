package id.chalathadoa.myapplication.tugastopik4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.chalathadoa.myapplication.R
import id.chalathadoa.myapplication.databinding.ActivityMainBinding
import id.chalathadoa.myapplication.databinding.ActivitySplashScreenBinding

class ActivitySplashScreen : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler().postDelayed({
            startActivity(Intent(this,ActivityLogin::class.java))
            finish()
        }, 3000)
    }
}