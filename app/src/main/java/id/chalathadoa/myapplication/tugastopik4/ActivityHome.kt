package id.chalathadoa.myapplication.tugastopik4

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import id.chalathadoa.myapplication.R
import id.chalathadoa.myapplication.databinding.ActivityHomeBinding

class ActivityHome : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    lateinit var preferences: SharedPreferences
    private lateinit var tvUsername: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //mendapatkan data
        preferences = getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
        val username = preferences.getString("USERNAME", "")
        binding.tvWelcomeHome.text = username

        tvUsername = findViewById(R.id.tv_welcome_home)
        if(intent.extras != null){
            val bundle = intent.extras
            tvUsername.setText("Welcome, ${bundle?.getString("username")} !")
        } else {
            tvUsername.setText("Anda Belum Login!")
        }

        binding.btnLogout.setOnClickListener {
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.clear()
            editor.apply()
            Toast.makeText(this, "Anda telah Logout!", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ActivityLogin::class.java)
            startActivity(intent)
            finish()
        }
    }
}