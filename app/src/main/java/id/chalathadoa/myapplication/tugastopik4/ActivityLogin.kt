package id.chalathadoa.myapplication.tugastopik4

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.SyncStateContract
import android.text.method.HideReturnsTransformationMethod
import android.widget.EditText
import android.widget.Toast
import id.chalathadoa.myapplication.MainActivity
import id.chalathadoa.myapplication.R
import id.chalathadoa.myapplication.databinding.ActivityLoginBinding
import id.chalathadoa.myapplication.databinding.ActivityMainBinding

class ActivityLogin : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    lateinit var sharPref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //shared preference
        sharPref = getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

        binding.btnSubmitLogin.setOnClickListener {
            val username = binding.etUsernameLogin.text.toString()
            val password = binding.etPasswordLogin.text.toString()

            val editor: SharedPreferences.Editor = sharPref.edit()
            editor.putString("USERNAME", username)
            editor.putString("USERNAME", password)
            editor.apply()

            if (username.isEmpty()|| password.isEmpty()){
                Toast.makeText(this, "Silakan masukkan Username dan Password Terlebih Dahulu!", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            } else {
                Toast.makeText(this, "Welcome $username !", Toast.LENGTH_SHORT).show()
            }
            //startActivity(Intent(this,ActivityLogin::class.java))
            val bundleNama = Bundle()
            bundleNama.putString("username", binding.etUsernameLogin.text.toString())

            val intent = Intent(this, ActivityHome::class.java)
            intent.putExtras(bundleNama)
            startActivity(intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
    }
}