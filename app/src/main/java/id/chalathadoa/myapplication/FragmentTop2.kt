package id.chalathadoa.myapplication

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.chalathadoa.myapplication.databinding.FragmentTop2Binding

class FragmentTop2 : Fragment() {
    private var _binding: FragmentTop2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTop2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        alertDialogStandard()
        alertDialogAction()
        alertDialogCustom()
        dialogFragement()
    }
    private fun alertDialogStandard(){
        binding.btnAlertSTANDARD.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Ini Alert Dialog STANDARD")
            dialog.setMessage("Alert Dialog jenis ini biasanya digunakan untuk menampilkan pesan yang tidak terlalu urgent. Dimana user tidak diberi pilihan tombol.")
            dialog.setCancelable(true)
            dialog.show()
        }
    }
    private fun alertDialogAction(){
        binding.btnAlertAKSI.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Ini Alert Dialog DENGAN AKSI")
            dialog.setMessage("Alert Dialog ini memiliki aksi yang mana bisa dipilih oleh user. Jika setCancelablenya di false, maka mau tidak mau user harus memilih salah satu action tersebut.")
            dialog.setCancelable(false)
            dialog.setPositiveButton("Yes"){dialogInterface, _ ->
                createToast("Terimakasih telah memilih POASITIVE BUTTON").show()
            }
            dialog.setNegativeButton("No"){dialogInterface, _ ->
                createToast("Anda telah memilih NEGATIVE BUTTON").show()
            }
            dialog.setNeutralButton("I'm netral"){dialogInterface, _ ->
                createToast("You're Neutral").show()
            }
            dialog.show()
        }
    }
    private fun alertDialogCustom(){
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.layout_custom, null, false)
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setView(view)

        val dialog = dialogBuilder.create()
        binding.btnAlertCUSTOM.setOnClickListener {
            createToast("Custom Dialog is closed").show()
            dialog.dismiss()
        }
        dialog.show()
    }
    private fun dialogFragement(){
        binding.btnDialogFragment.setOnClickListener {
            val dialogFragment = myDialogFragment()
            dialogFragment.show(childFragmentManager, null)
        }
    }
    private fun createToast(message: String): Toast{
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }
}