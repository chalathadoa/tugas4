package id.chalathadoa.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.chalathadoa.myapplication.databinding.ActivityMainBinding
import id.chalathadoa.myapplication.tugastopik4.ActivityLogin
import id.chalathadoa.myapplication.tugastopik4.FragmentLogin

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setTheme(R.style.Theme_MyApplication)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Handler().postDelayed({
            startActivity(Intent(this,ActivityLogin::class.java))
            finish()
        }, 3000)
    }
}