package id.chalathadoa.myapplication

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.chalathadoa.myapplication.adapter.AplikasiAdapter
import id.chalathadoa.myapplication.databinding.FragmentTop4Binding
import id.chalathadoa.myapplication.model.aplikasi

class FragmentTop4 : Fragment() {

    private var _binding: FragmentTop4Binding? = null
    private val binding get() = _binding!!

    //membuat variabel global
    //shrusnya tdk boleh membuat shared preferences dalam variabel global sprt ini karena akan errorr.
    //ini untuk multiple shared preferences dgn perbedaannya adalah id
//    private val sharedPref = requireContext().getSharedPreferences("id_person", Context.MODE_PRIVATE)

    private lateinit var sharedPref: SharedPreferences
    //ini untuk 1 shared preferences.
//    private val singleSharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE)

    //ini untuk shared preference dan editornya lgsg.
    //private val sharedPref = requireContext().getSharedPreferences("id_person", Context.MODE_PRIVATE).edit()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTop4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = requireContext().getSharedPreferences("id_person", Context.MODE_PRIVATE)
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ini untuk menaruh datanya
//        val sharedPrefEditor = sharedPref.edit()

        btnSaveClick()
        btnViewClick()
        btnClearClick()
    }
    private fun btnSaveClick(){
        binding.btnSave.setOnClickListener {
            //mengambil value dari edit text
            //karena id untuk integer, maka menambahkan toInt()
            val id = binding.etInputId.text.toString().toInt()
            val nama = binding.etInputName.text.toString()

            //membuat editor
            val editor = sharedPref.edit()

            //menaruh value ke sharedPref
            editor.putInt("id", id)
            editor.putString("nama", nama)
            //untuk menyimpan ke dalam sharedPref
            editor.apply()

            Toast.makeText(requireContext(), "Berhasil disimpan", Toast.LENGTH_SHORT).show()
        }
    }
    private fun btnViewClick(){
        binding.apply {
            btnView.setOnClickListener{

                //mendapatkan value dari sharedPref
                val id = sharedPref.getInt("id", 123)
                val nama = sharedPref.getString("nama", "default name")

                //menampilkan dalam text view
                tvShowId.text = id.toString()
                tvShowName.text = nama.toString()
            }
        }
    }
    private fun btnClearClick(){
        binding.btnClear.setOnClickListener {
            val editor = sharedPref.edit()
            editor.clear()
            editor.apply()
            binding.tvShowName.setText("")
            binding.tvShowId.setText("")

            Toast.makeText(requireContext(), "Data telah dihapus!", Toast.LENGTH_LONG).show()
        }
    }
}