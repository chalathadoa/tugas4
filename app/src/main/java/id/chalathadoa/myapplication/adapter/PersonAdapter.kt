package id.chalathadoa.myapplication.adapter

import android.app.Person
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.chalathadoa.myapplication.R
import id.chalathadoa.myapplication.model.person

class PersonAdapter(val listPerson: ArrayList<person>): RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {

    //class yg fokus ke per itemnya
    inner class PersonViewHolder(view: View): RecyclerView.ViewHolder(view){
        val tvNama: TextView = view.findViewById(R.id.tv_item_nama)
        val tvPhone: TextView = view.findViewById(R.id.tv_item_phone)

        fun bind(item: person){

            tvNama.text = item.nama
            tvPhone.text = item.phone.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_person, parent, false)
        return PersonViewHolder(view)
    }

    //untuk penghubung
    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.bind(listPerson[position])
    }

    override fun getItemCount(): Int = listPerson.size
}