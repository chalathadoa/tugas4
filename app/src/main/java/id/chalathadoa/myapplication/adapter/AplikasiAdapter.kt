package id.chalathadoa.myapplication.adapter

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.chalathadoa.myapplication.R
import id.chalathadoa.myapplication.model.aplikasi
import id.chalathadoa.myapplication.model.person

class AplikasiAdapter(val listAplikasi: ArrayList<aplikasi>):RecyclerView.Adapter<AplikasiAdapter.AplikasiViewHolder>() {

    //pengunaan diffutil
    private val diffCallback = object : DiffUtil.ItemCallback<aplikasi>() {
        override fun areItemsTheSame(oldItem: aplikasi, newItem: aplikasi): Boolean {
            //kenapa menggunakan item nama? karena yg dapat membedakan apk satu dgn yg lain adalah nama.
            //jika misal ada item id, maka yg digunakan adalah id.
            return oldItem.nama == newItem.nama
        }

        override fun areContentsTheSame(oldItem: aplikasi, newItem: aplikasi): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
            //return oldItem == newItem
        }
    }

    //deklarasi asynclistdiffer untuk menerima nilai secara livedata
    private val differ = AsyncListDiffer(this,diffCallback)

    //function dibawah untuk memnggil adapter yg telah dibuat dan memasukkan data
    //dari activity atau fragment yg menggunakannya.
    fun submitData(value: ArrayList<aplikasi>) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AplikasiViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_aplikasi, parent, false)
        return AplikasiViewHolder(view)
    }

    //untuk penghubung
    override fun onBindViewHolder(holder: AplikasiViewHolder, position: Int) {
        //untuk menggnakan diffutil, semua listAplikasi diganti dengan differ.currentlist
        val data = differ.currentList[position]
        holder.bind(differ.currentList[position])

        //cara lama
        //holder.bind(listAplikasi[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    //atau juga bisa
    /**
     * override fun getItemCount(): Int {
     *     //cara lama
     *     return listAplikasi.size
     *
     *     //cara baru
     *     return differ.currentList.size
     * }
     */

    //class yg fokus ke per item nya
    inner class AplikasiViewHolder(view: View): RecyclerView.ViewHolder(view){
        val imgIconApp: ImageView = view.findViewById(R.id.img_item_icon)
        val tvNamaApp: TextView = view.findViewById(R.id.tv_item_namaApp)
        val tvPendiriApp: TextView = view.findViewById(R.id.tv_item_pendiriApp)

        fun bind(item: aplikasi){
            //imgIconApp.setImageResource(item.icon)
            tvNamaApp.text = item.nama
            tvPendiriApp.text = item.pendiri

            Glide.with(itemView.context)
                .load(item.icon)
                .into(imgIconApp)
        }
    }
}