package id.chalathadoa.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import id.chalathadoa.myapplication.adapter.AplikasiAdapter
import id.chalathadoa.myapplication.databinding.FragmentTugasTop3Binding
import id.chalathadoa.myapplication.model.aplikasi


class FragmentTugasTop3 : Fragment() {
    private var _binding: FragmentTugasTop3Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTugasTop3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAplikasi()
    }
    private val listAplikasi = arrayListOf<aplikasi>(
        aplikasi(R.drawable.ic_fb, "Facebook", "Mark Zukerberg"),
        aplikasi(R.drawable.ic_hago,"Hago", "Neotasks inc"),
        aplikasi(R.drawable.ic_ig, "Instagram", "Kevin Systrom"),
        aplikasi(R.drawable.ic_linkaja,"Linkaja", "PT  Fintek Karya Nusantara"),
        aplikasi(R.drawable.ic_linkedin,"LinkedIn", "LinkedIn Corporation"),
        aplikasi(R.drawable.ic_netfix,"Netflix", "Reed Hasting dan Marc Randolph"),
        aplikasi(R.drawable.ic_shopi,"Shopee", "Forrest Li"),
        aplikasi(R.drawable.ic_snackvid,"Snack Video", "Su Hua Cheng Yixiao"),
        aplikasi(R.drawable.ic_telegram,"Telegram", "Pavel Durov dan Nikolai Durov"),
        aplikasi(R.drawable.ic_tokped,"Tokopedia", "GoTo Group"),
        aplikasi(R.drawable.ic_viu,"Viu", "PCCW Media"),
    )
    private fun initAplikasi(){
        val AplikasiAdapter = AplikasiAdapter(listAplikasi)
        binding.rvAplikasi.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = AplikasiAdapter
        }
    }

    private fun updateDataWithDiffUtil(value: ArrayList<aplikasi>){
        //AplikasiAdapter.submitData(value)
    }
}