package id.chalathadoa.myapplication

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import id.chalathadoa.myapplication.databinding.FragmentMyDialogBinding

class myDialogFragment : DialogFragment() {
    private var _binding: FragmentMyDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMyDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvJudulDialogFragment.text = "Alert dengan DIALOG FRAGMENT"
            tvIsiDialogFragment.text = "@string/isi_dialog_fragment"
            btnFragdialog1.text = "iya"
            btnFragdialog2.text = "THANKS"
        }
        binding.btnFragdialog1.setOnClickListener {
            dialog?.dismiss()
            createToast("Terimakasih")
        }
        binding.btnFragdialog2.setOnClickListener {
            dialog?.dismiss()
            createToast("Terimakasih kembali")
        }
    }
    private fun createToast(message: String): Toast{
        return Toast.makeText(requireContext(),message, Toast.LENGTH_SHORT)
    }
}