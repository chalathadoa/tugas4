package id.chalathadoa.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import id.chalathadoa.myapplication.databinding.FragmentTop1Binding

class FragmentTop1 : Fragment() {
    private var _binding: FragmentTop1Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTop1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        showSnackbar()
    }
    private fun showToast(){
        var nama = binding.etNama
        binding.btnTampilToast.setOnClickListener {
            Toast.makeText(requireContext(),"Selamat Datang ${nama.text.toString()}", Toast.LENGTH_SHORT).show()
        }
    }
    private fun showSnackbar(){
        binding.btnTampilSnackbar.setOnClickListener {
            Snackbar.make(it,"Ini Snackbar", Snackbar.LENGTH_INDEFINITE)
                .setAction("Pilih"){
                    Toast.makeText(requireContext(), "Terimakasih telah memilih", Toast.LENGTH_SHORT).show()
                }
                .show()
        }
    }
}